module.exports = ({file, options, env}) => ({
  plugins: {
    "postcss-font-family-system-ui":true,
    "postcss-icss-values": true,
    "postcss-cssnext": true,
  }
});