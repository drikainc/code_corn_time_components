import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './main.css';

function Main({ items = [] }) {
  return (
    <div>
      { items.map(child => child) }
    </div>
  );
}

export default CSSModules(Main, styles);
