import React from 'react';
import CSSModules from 'react-css-modules';

import FaCog from 'react-icons/lib/fa/cog';
import FaHeart from 'react-icons/lib/fa/heart';
import FaInfoCircle from 'react-icons/lib/fa/info-circle';

import MenuItem from '../components/menu/item';
import IconItem from '../components/menu/icon';
import SearchItem from '../components/search/search';
import DetailPoster from '../components/detail/poster';
import DetailContainer from '../components/detail/container';

import styles from './main.css';
import Button from '../components/button/button';


function Main() {
  const itemsLeft = [
    <MenuItem {...{ name: 'Movies', focused: true }} />,
    <MenuItem {...{ name: 'Series', focused: false }} />,
  ];
  const itemsRight = [
    <SearchItem {...{ expanded: true }} />,
    <SearchItem />,
    <IconItem {...{ children: [<FaCog />] }} />,
    <IconItem {...{ children: [<FaHeart />] }} />,
    <IconItem {...{ children: [<FaInfoCircle />] }} />,
    <IconItem {...{ focused: true, children: [<FaCog />] }} />,
  ];
  const config = {
    containerImgSrc:"http://image.tmdb.org/t/p/w500/n1y094tVDFATSzkTnFxoGZ1qNsG.jpg",
    imgSrc:"http://image.tmdb.org/t/p/w500/inVq3FRqcYIRl2la8iZikYYxFNR.jpg"
  };
  const posters= {
    // children: posterMock.map( posterConfig => <Poster {...posterConfig} />)
    items: [
      <DetailContainer {...{imgSrc:config.containerImgSrc,posterItem:<DetailPoster {...config} />}}/>
    ]
  };
  return (
    <div onScroll={(event)=>{console.log(event)}}>
      <Button {...{title: "Watch Now"}} />
      <Button {...{title: "Watch Now Focused", focused: true}}/>
      {/*<Menu {...{itemsLeft, itemsRight}}></Menu>*/}
      {/*<Container  {...posters} />*/}
    </div>
  );
}

export default CSSModules(Main, styles);
