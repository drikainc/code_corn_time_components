import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Main from './main/main';

window.onload = ()=>{


  const render = Component => {
    ReactDOM.render(
      <AppContainer>
        <Component />
      </AppContainer>,
      document.getElementById('content')
    );
  }

  render(Main);

  if (module.hot) {
    module.hot.accept('./main/main.jsx', () => {
      const NextRootMain = require('./main/main').default;
      render(NextRootMain);
    });
  }
}
