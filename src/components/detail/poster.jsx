import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './poster.css';

const DetailPosterComponent = ( {imgSrc} ) => {
  console.log(imgSrc);
  return (
    <div styleName="poster-detail" style={{ backgroundImage: `url(${imgSrc})` }} />
  );
}

export default CSSModules(DetailPosterComponent, styles);