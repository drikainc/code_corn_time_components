import React from 'react';
import CSSModules from 'react-css-modules';

import styles from './container.css';

const DetailPosterContainer = ({
  imgSrc,
  id,
  title
}) => (
  <div>
    <div
      styleName="detail-container-background"
      style={{ backgroundImage: `url(${imgSrc})` }}
    />
    <div styleName="detail-container-background-overlap" />
    <div styleName="detail-container-content">
      <div styleName="detail-container-content-wrapper">
      </div>
    </div>
  </div>
);
export default CSSModules(DetailPosterContainer, styles);
