import React from 'react';
import CSSModules from 'react-css-modules';
import FaEye from 'react-icons/lib/fa/eye';
import FaHeart from 'react-icons/lib/fa/heart';
import FaClose from 'react-icons/lib/fa/close';

import styles from './container.css';

import Button from  '../button/button';

const DetailPosterContainer = ({
                                 imgSrc,
                                 id,
                                 genres = [],
                                 runtime,
                                 posterItem,
                                 title,
                                 year,
                                 synopsis,
                                 onWatchButtonEnter = function () {
                                 },
                                 onWatchButtonLeave = function () {
                                 },
                                 onWatchButtonClick = function () {
                                 },
                                 onTrailerButtonEnter = function () {
                                 },
                                 onTrailerButtonLeave = function () {
                                 },
                                 onTrailerButtonClick = function () {
                                 },
                                 onSeenButtonEnter = function () {
                                 },
                                 onSeenButtonLeave = function () {
                                 },
                                 onSeenButtonClick = function () {
                                 },
                                 onBookmarkButtonEnter = function () {
                                 },
                                 onBookmarkButtonLeave = function () {
                                 },
                                 onBookmarkButtonClick = function () {
                                 },
                                 bookmarkButtonFocused = false,
                                 bookmarked = false,
                                 seen = false,
                                 seenButtonFocused = false,
                                 watchButtonFocused = false,
                                 trailerButtonFocused = false,
                                 onBackClick = function () {
                                 },
                               }) => {

  const bookmarkStyleName = (bookmarkButtonFocused) ? "button-icon-focused" : "button-icon";
  const seenStyleName = (seenButtonFocused) ? "button-seen-icon-focused" : "button-icon";

  const bookmarkedButton = (bookmarked) ? "button-icon-selected" : "button-icon";
  const seenButton = (seen) ? "button-seen-icon-selected" : "button-icon";

  return (
    <div>
      <div styleName="detail-container-background"
           style={{backgroundImage: `url(${imgSrc})`}}/>
      <div styleName="detail-container-background-overlap"/>
      <div styleName="detail-container-content">
        <div styleName="detail-container-content-wrapper">
          {posterItem}
          <div styleName="detail-container-text-info">
            <div styleName="back-button" onClick={() => onBackClick()}>
              <FaClose/></div>
            <h1 styleName="detail-title">{title}</h1>
            <div>{[year, `${runtime} min`, genres.join('/')].join(' \u00b7 ')}</div>
            <p>{synopsis}</p>
            <div styleName="detail-actions">
              <div styleName="control-actions">
                <div styleName="text-button"
                     onMouseEnter={() => onBookmarkButtonEnter()}
                     onMouseLeave={() => onBookmarkButtonLeave()}
                     onClick={() => {
                       onBookmarkButtonClick(id)
                     }}>
                  {
                    (bookmarked) ? (<span><FaHeart
                      styleName={bookmarkedButton}/>Bookmarked</span>) : (
                      <span><FaHeart styleName={bookmarkStyleName}/>Not bookmarked</span>)
                  }
                </div>
                <div styleName="text-button"
                     onMouseEnter={() => onSeenButtonEnter()}
                     onMouseLeave={() => onSeenButtonLeave()} onClick={() => {
                  onSeenButtonClick(id)
                }}>
                  {
                    (seen) ? (
                      <span><FaEye styleName={seenButton}/>Seen</span>) : (
                      <span><FaEye styleName={seenStyleName}/>Not seen</span>)
                  }
                </div>
              </div>
              <div styleName="control-actions">
                <Button {...{
                  title: "Watch Now",
                  focused: watchButtonFocused,
                  onEnter: onWatchButtonEnter,
                  onLeave: onWatchButtonLeave,
                  onClick: () => onWatchButtonClick(id)
                }} />
                <Button {...{
                  title: "Watch Trailer",
                  focused: trailerButtonFocused,
                  onEnter: onTrailerButtonEnter,
                  onLeave: onTrailerButtonLeave,
                  onClick: () => onTrailerButtonClick(id)
                }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CSSModules(DetailPosterContainer, styles);