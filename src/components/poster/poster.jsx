import React from 'react';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import { FaHeart, FaEye } from 'react-icons/lib/fa';

import styles from './poster.css';

function PosterComponent({
  id, imgSrc, title, year, userFocused, rating, bookmarked, seen,
  onSelect = function () {}, onSee = function () {}, onBookmark = function () {}, onFocus = function () {},
}) {
  const backgroundStyle = { backgroundImage: `url(${imgSrc})` };
  const contentUsed = (userFocused || bookmarked || seen);
  const contentFocusStyle = (userFocused) ? 'content-focused' : 'content';
  const contentUsedStyle = (contentUsed) ? 'poster-focused' : 'poster';
  const actionBarStyle = (contentUsed) ? 'actions-focused' : 'actions';
  const itemSeenStyle = (seen) ? 'seen-item' : 'action-item';
  const itemBookmarkedStyle = (bookmarked) ? 'bookmarked-item' : 'action-item';

  return (
    <div styleName="container" onClick={(event) => {event.stopPropagation(); onSelect(id)}} onMouseEnter={(event) => { event.stopPropagation(); onFocus(id); }} onMouseLeave={(event) => { event.stopPropagation(); onFocus(id); }} >
      <div styleName="content-wrapper">
        <div styleName={contentFocusStyle} >
          <div styleName={actionBarStyle}>
            <div styleName="action-wrapper" onClick={(event) =>{event.stopPropagation(); onSee(id)}}>
              <FaEye styleName={itemSeenStyle} />
            </div>
            <div styleName="action-wrapper" onClick={(event) =>{event.stopPropagation(); onBookmark(id)}}>
              <FaHeart styleName={itemBookmarkedStyle} />
            </div>
          </div>
        </div>
        <div styleName={contentUsedStyle} style={backgroundStyle}/>
        <div styleName="poster-loading"/>
        <div styleName="text-wrapper">
          <div styleName="title">{title}</div>
          <div styleName="year">{year}</div>
        </div>
      </div>
    </div>
  );
}

PosterComponent.propTypes = {
  imgSrc: PropTypes.string,
  title: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  userFocused: PropTypes.bool,
  rating: PropTypes.number,
  bookmarked: PropTypes.bool,
  seen: PropTypes.bool,
};

PosterComponent.defaultProps = {
  // TODO: should be a base64 img
  imgSrc: '',
  userFocused: false,
  rating: -1,
  bookmarked: false,
  seen: false,
};

export default CSSModules(PosterComponent, styles);
