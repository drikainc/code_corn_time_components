import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './container.css';

function Container({ items = [], loading = false }) {
  return (
    <div styleName="container">
      {items}
    </div>
  );
}

export default CSSModules(Container, styles);
