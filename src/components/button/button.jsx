import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './button.css';

const Button = ({title,focused=false, onClick = function () {
}, onEnter = function () {
}, onLeave = function () {
}}) => {
  const className=(!focused)?"container":"container-enter"
  return (
    <div styleName={className} onClick={()=>onClick()} onMouseEnter={()=>onEnter()} onMouseLeave={()=>onLeave()}>
      {title}
    </div>
  )
}

export default CSSModules(Button, styles);