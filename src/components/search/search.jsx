import React from 'react';
import CSSModules from 'react-css-modules';
import FaSearch from 'react-icons/lib/fa/search'
import styles from './search.css';

function SearchComponent({id, expanded = false, onClick = function(){}, onEnter=function(){}, onLeave = function(){}, onBlur= function(){}}){
  const expandedComponent = (
    <div styleName="input-container">
      <input placeholder="Search" type="text" styleName="component-input" maxLength="15" autoFocus="true" onBlur={()=>onBlur(id)}/>
      <FaSearch styleName="input-icon" />
    </div>
  );
  const collapsedComponent = (
    <FaSearch/>
  );
  return (
    <div styleName="container" onClick={()=>onClick(id)} onMouseEnter={()=>onEnter(id)} onMouseLeave={()=>onLeave(id)}>
      { (expanded) ? expandedComponent : collapsedComponent }
    </div>
  );
}

export default CSSModules(SearchComponent, styles);