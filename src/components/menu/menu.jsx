import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './menu.css';

function MenuComponent({itemsLeft=[], itemsRight=[]}) {
  return (
    <div styleName="container">
      <div>
        {itemsLeft.map((item,key) => React.cloneElement(item, {key}))}
      </div>
      <div styleName="right-items">
        {itemsRight.map((item,key) => React.cloneElement(item, {key}))}
      </div>
    </div>
  );
}

export default CSSModules(MenuComponent, styles);
