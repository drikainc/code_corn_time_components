import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './icon.css';

function ItemIconComponent({items=[],id,path, focused, selected=false, onEnter, onLeave, onClick}) {
  const containerName = (focused||selected)?'container-focused':'container';
  return (
    <div styleName={containerName} onMouseEnter={()=>onEnter(id)} onMouseLeave={()=>onLeave(id)} onClick={()=>onClick(id, path)}>
      {items.map(child => child)}
    </div>
  );
}

export default CSSModules(ItemIconComponent, styles);
