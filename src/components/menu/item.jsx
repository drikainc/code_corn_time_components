import React from 'react';
import CSSModules from 'react-css-modules';

import styles from './item.css';

function MenuItemComponent({id, path, name, focused=false, selected=false, onEnter=function(){}, onLeave=function(){}, onClick=function(){}}){
  console.log(`selected: ${selected}`)
  const containerName = (focused || selected)?'container-focused':'container';
  return (
    <div onClick={()=>onClick(id, path)} onMouseEnter={()=>onEnter(id)} onMouseLeave={()=>onLeave(id)} styleName={containerName}>
      {name}
    </div>
  );
}
export default CSSModules(MenuItemComponent, styles);